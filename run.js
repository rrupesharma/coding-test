const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let inputData = ""
let country = ['uk','germany']
let processData = {};

let ukglovesstock = 100
let germanyglovesstock = 50
let ukmaskstock = 100
let germanymaskstock = 100

let ukglovesRate = 100
let germanyglovesRate = 150
let ukmaskRate = 65
let germanymaskRate = 100


processData.inventory = {
    'uk':{
        gloves : {
            stock : ukglovesstock,
            rate : ukglovesRate
        },
        mask : {
            stock : ukmaskstock,
            rate : ukmaskRate
        }
    },
    'germany':{
        gloves : {
            stock : germanyglovesstock,
            rate : germanyglovesRate
        },
        mask : {
            stock : germanymaskstock,
            rate : germanymaskRate
        }
    }
}

//setting shipping cost as per password
processData.shipping = {
    costOfShippingOtherCountryOfPerTenUnit : 400,
    costOfShippingSameCountryOfPerTenUnit : 320,
}

processData.passport = {}
let passport = {
    'B' : [3,2,7],
    'A' : [3,2,9]
}

result = {}
result.itemToltalCost = 0;
result.stockLeft = {
    'uk':{
        gloves : ukglovesstock,
        mask : ukmaskstock,
    },
    'germany':{
        gloves : germanyglovesstock,
        mask : germanymaskstock,
    }
}

let getsellcostofItem = (item,country,order,result)=>{
    
    let country2 = country == 'uk' ? 'germany' : 'uk'
    let itemCost = processData.inventory[country][item].rate
    let itemStock = processData.inventory[country][item].stock
    let itemStock2 = processData.inventory[country2][item].stock
    let itemOrder = order <= itemStock2 ? order : itemStock2;
    let itemToltalCost = 0
    let itemOrderLeft = 0
    let shippingCostPerTenUnit = processData.passport.country != null &&  processData.passport.country == country ? processData.shipping.costOfShippingSameCountryOfPerTenUnit : processData.shipping.costOfShippingOtherCountryOfPerTenUnit;
    let shippingCost = 0
    let stockLeft = 0
    if(result.stockLeft[country][item]!=0){
        if(itemOrder<=itemStock){
            let letItem = itemOrder%10;
            if(letItem>0 && itemOrder > 10){
                itemOrder = parseInt(itemOrder/10) * 10
                shippingCost = processData.country == country? 0 : shippingCostPerTenUnit*(parseInt(itemOrder/10))
                itemToltalCost = (itemOrder * itemCost) + shippingCost
                stockLeft = processData.inventory[country][item].stock - itemOrder
                itemOrderLeft = letItem
            }else{
                shippingCost = processData.country == country ? 0 : itemOrder >= 10 ? shippingCostPerTenUnit * (parseInt(itemOrder/10)):shippingCostPerTenUnit ;
                itemToltalCost = (itemOrder * itemCost) + shippingCost
                stockLeft = processData.inventory[country][item].stock - itemOrder
                itemOrderLeft = order >= itemStock2? order - itemStock2:order - itemStock
            }
            
        }else {
            itemOrder = itemStock
            shippingCost = processData.country == country? 0 : shippingCostPerTenUnit*(parseInt(itemOrder/10))
            itemToltalCost = (itemOrder * itemCost) + shippingCost
            itemOrderLeft = order - itemStock
            stockLeft = processData.inventory[country][item].stock - itemStock
        }
    }
   
    let results =  {
        itemToltalCost:itemToltalCost,
        itemOrderLeft:itemOrderLeft,
        shippingCost:shippingCost,
        stockLeft:stockLeft,
        country:country,
        country2:processData.country,
        item:item,
        itemOrder: itemOrder,
        shippingCostPerTenUnit:shippingCostPerTenUnit
    }

    return results;
}

let runLoop = (item,order)=>{
    let case1 = getsellcostofItem(item,'uk',order,result)
    let case2 = getsellcostofItem(item,'germany',order,result)
    //console.log(case1,case2)
    if(case1.itemToltalCost!=0 && case1.itemToltalCost < case2.itemToltalCost){
        result.itemToltalCost = result.itemToltalCost + case1.itemToltalCost
        result.stockLeft[case1.country][item] = result.stockLeft[case1.country][item] - case1.itemOrder
        if(case1.itemOrderLeft > 0){
            //console.log(item,case1.itemOrderLeft)
            runLoop(item,case1.itemOrderLeft)
        }
    }else{
        result.itemToltalCost = result.itemToltalCost + case2.itemToltalCost
        result.stockLeft[case2.country][item] = result.stockLeft[case2.country][item] - case2.itemOrder
        if(case2.itemOrderLeft > 0){
            //console.log(item,case2.itemOrderLeft)
            runLoop(item,case2.itemOrderLeft)
        }
    }
}

let run = (glove,mask)=>{
    runLoop('gloves',glove)
    runLoop('mask',mask)
}


let start = ()=>{
    rl.question("INPUT : ", function(name) {
        inputData = name
        let newData = inputData.split(":")
        newData = newData.map(v => v.toLowerCase())

        if(country.indexOf(newData[0].toLowerCase()) >- 1){
            //console.log(country.indexOf(newData[0].toUpperCase()),country[country.indexOf(newData[0].toUpperCase())])
            processData.country = country[country.indexOf(newData[0].toLowerCase())]
        }

        //check the passport format
        if(newData[1].toUpperCase().indexOf('B')==0){
            if(newData[1].length==1+passport['B'][0]+passport['B'][1]+passport['B'][2]){
                processData.passport.country = 'uk'
                processData.passport.validation = true
            }else {
                processData.passport.country = 'uk'
                processData.passport.validation = false
            }
        }else if(newData[1].toUpperCase().indexOf('A')==0){
            if(newData[1].length==1+passport['A'][0]+passport['A'][1]+passport['A'][2]){
                processData.passport.country = 'germany'
                processData.passport.validation = true
            }else {
                processData.passport.country = 'germany'
                processData.passport.validation = false
            }
        }else{
            processData.passport.country = null
            processData.passport.validation = true
        }

        //set input ordder
        processData.order = {}
        if(newData.indexOf('gloves') > -1){
            processData.order.gloves = parseInt(newData[newData.indexOf('gloves')+1])
        }
        if(newData.indexOf('mask') > -1){
            processData.order.mask = parseInt(newData[newData.indexOf('mask')+1])
        }


        if(!processData.passport.validation){
            //run(processData.order.gloves,processData.order.mask)
            console.log("OUTPUT : Invalide Passport"+":"+result.stockLeft.uk.mask+":"+result.stockLeft.germany.mask+":"+result.stockLeft.uk.gloves+":"+result.stockLeft.germany.gloves,JSON.stringify(processData))
        }else{
            if(processData.order.gloves <= ukglovesstock+germanyglovesstock && processData.order.mask <= ukmaskstock+germanymaskstock){
                run(processData.order.gloves,processData.order.mask)
                console.log("OUTPUT : "+ result.itemToltalCost+":"+result.stockLeft.uk.mask+":"+result.stockLeft.germany.mask+":"+result.stockLeft.uk.gloves+":"+result.stockLeft.germany.gloves)
            }else{
                console.log("OUTPUT : OUT_OF_STOCK"+":"+result.stockLeft.uk.mask+":"+result.stockLeft.germany.mask+":"+result.stockLeft.uk.gloves+":"+result.stockLeft.germany.gloves)
            }
            
        }
        rl.close();
    });
}


rl.on("close", function() {
    //start()
    //console.log("\nBYE BYE !!!");
    process.exit(0);
});

start()
